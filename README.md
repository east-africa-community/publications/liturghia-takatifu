# Liturghia Takatifu

This repository contains the MS Word document from which the 4th edition of _Liturghia Takatifu_ was printed in 2020.

The contents are:

- Typika

- Kairos

- Vesting Prayers

- Proskomede

- Sunday Matins

- Divine Liturgy of St John Chrysostom

- Octoechos for Sundays

- Memorial Service

To report problems with this publication please create an issue [here]()
